#!/bin/bash

./${BINARY_NAME} ${BINARY_ARGS} &
pid=$!

sleep 10s
kill $pid 2>&1 > /dev/null || true

wait $pid
retCode=$?

if [ $retCode -ne 0 ] && [ $retCode -ne 84 ]; then
    exit 1
fi
