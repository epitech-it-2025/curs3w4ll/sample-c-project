#!/usr/bin/env python3

from argparse import ArgumentParser

import gitlab

def red(val: str) -> str:
    return "\033[91m" + val + "\033[0m"

def yellow(val: str) -> str:
    return "\033[93m" + val + "\033[0m"

def cyan(val: str) -> str:
    return "\033[96m" + val + "\033[0m"

def green(val: str) -> str:
    return "\033[92m" + val + "\033[0m"

def parseArguments():
    parser = ArgumentParser(description="A program that automatically create a gitlab repository", epilog="Display the URL of the gitlab project and the ssh key to clone it")
    parser.add_argument("-l", type=str, choices=['c', 'cpp', 'other'], nargs=1, default='other', help="Code language used for the project")
    parser.add_argument("-g", type=str, nargs=3, metavar=("accessToken", "projectURL", "username"), help="Github information for repository mirroring")
    parser.add_argument("projectName", type=str, nargs=1, help="Name of the project")
    parser.add_argument("gitlabGroup", type=str, nargs='?', default="epitech-it-2025", help="Gitlab groupe where the repository will be located, default: 'epitech-it-2025'")
    parser.add_argument("gitlabToken", type=str, nargs=1, help="Gitlab personnal access token")
    parser.add_argument("binaryName", type=str, nargs=1, help="Name of the binary of the project to execute")
    parser.add_argument("binaryArguments", type=str, nargs=1, help="Arguments to pass during the binary call")
    parser.add_argument("-i", action="store_true", help="Ignore when errors occured")
    return parser.parse_args()

def setupCI(args, project):
    if "c" in args.l:
        f = open("./templates/c/.gitlab-ci.yml", "r")
        gitlabciContent = f.read()
        project.files.create({'file_path': '.gitlab-ci.yml', 'branch': 'dev', 'content': gitlabciContent, 'author_email': 'ci.epitech@gmail.com', 'author_name': 'CI Epitech', 'commit_message': 'Setup'})
        f = open("./templates/c/.ci/mirroring-checker.sh", "r")
        ciFileContent = f.read()
        project.files.create({'file_path': '.ci/mirroring-checker.sh', 'branch': 'dev', 'content': ciFileContent, 'author_email': 'ci.epitech@gmail.com', 'author_name': 'CI Epitech', 'commit_message': 'Setup'})
        f = open("./templates/c/.ci/myscript.sh", "r")
        ciFileContent = f.read()
        project.files.create({'file_path': '.ci/myscript.sh', 'branch': 'dev', 'content': ciFileContent, 'author_email': 'ci.epitech@gmail.com', 'author_name': 'CI Epitech', 'commit_message': 'Setup'})


def main():
    args = parseArguments()

    try:
        gl = gitlab.Gitlab(private_token=args.gitlabToken[0])

        groups = gl.groups.list(search=args.gitlabGroup)
        if len(groups) < 1:
            error = red("No group found with name: " + args.gitlabGroup)
            print(error, file=stderr)
            exit(1)
        groupId = groups[0].get_id()
        groupInfo = gl.groups.get(groupId)
        print(cyan("Using group '" + groupInfo.full_path + "'..."))

        print(cyan("Create a new project with the name '" + args.projectName[0] + "' in group '" + groupInfo.name + "'..."))
        try:
            project = gl.projects.create({'name': args.projectName[0], 'namespace_id': groupId})
        except gitlab.exceptions.GitlabCreateError as e:
            error = red("Cannot create the project: " + str(e.error_message))
            print(error, file=stderr)
            exit(1)
        print(green("Project created with path '" + project.path + "'"))

        try:
            project.branches.create({"branch": "dev", "ref": "main"})
        except gitlab.exceptions.GitlabCreateError as e:
            if args.i:
                error = yellow("Cannot create dev branch: " + str(e.error_message))
            else:
                error = red("Cannot create dev branch: " + str(e.error_message))
                delGitlabSnippets(project, args, gitlabSnippets)
                project.delete()
            print(error, file=stderr)
            if not args.i:
                exit(1)

        if args.g:
            mirrorUrl = args.g[1]
            if (mirrorUrl.startswith("https://")):
                mirrorUrl = args.g[1][8:]
            print(cyan("\nEnabling repository mirroring on '" + args.g[2] + ":@" + mirrorUrl + "'..."))
            try:
                project.remote_mirrors.create({'url': "https://" + args.g[2] + ":" + args.g[0] + "@" + mirrorUrl, 'password': args.g[1], 'enabled': True})
            except gitlab.exceptions.GitlabCreateError as e:
                if (args.i):
                    error = yellow("Cannot add repository mirroring: " + str(e.error_message))
                else:
                    error = red("Cannot add repository mirroring: " + str(e.error_message))
                    delGitlabSnippets(project, args, gitlabSnippets)
                    project.delete()
                print(error, file=stderr)
                if (not args.i):
                    exit(1)
            print(green("Repository mirroring enabled"))

        try:
            project.variables.create({"key": "BINARY_NAME", "value": args.binaryName[0]})
        except gitlab.exceptions.GitlabCreateError as e:
            if (args.i):
                error = yellow("Cannot add ci variable: " + str(e.error_message))
            else:
                error = red("Cannot add ci variable: " + str(e.error_message))
                delGitlabSnippets(project, args, gitlabSnippets)
                project.delete()
            print(error, file=stderr)
            if (not args.i):
                exit(1)
        try:
            project.variables.create({"key": "BINARY_ARGS", "value": args.binaryName[0]})
        except gitlab.exceptions.GitlabCreateError as e:
            if (args.i):
                error = yellow("Cannot add ci variable: " + str(e.error_message))
            else:
                error = red("Cannot add ci variable: " + str(e.error_message))
                delGitlabSnippets(project, args, gitlabSnippets)
                project.delete()
            print(error, file=stderr)
            if (not args.i):
                exit(1)

        setupCI(args, project)
        print(green("\nYour project has been successfully created !\nWeb link: " + project.web_url + "\nClone with ssh: git clone " + project.ssh_url_to_repo))
    except gitlab.exceptions.GitlabAuthenticationError as e:
        print(red("Cannot authenticate to gitlab with gived personnal access token:"), e, file=stderr)
        exit(1)



if __name__ == "__main__":
    exit(main())
