#!/bin/bash

out=$(curl --header "PRIVATE-TOKEN: $CI_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/remote_mirrors" | jq | grep "update_status" | perl -lape 's/.*".*".* "(.*)".*/$1/g')
if [ "$out" != "finished" ] ; then
    exit 1
fi
